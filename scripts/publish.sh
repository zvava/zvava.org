#!/bin/sh
if [ "$(hostname)" != "zdelegate2" ]; then
	ssh zvava@zvava.org "bash /home/zvava/zvava.org/scripts/publish.sh"
else
	cd /home/zvava/zvava.org
	echo "-> fetching latest updates"
	git fetch --all
	git reset --hard origin/master
	git pull
	chmod +x scripts/publish.sh
	echo "-> running generator script"
	/usr/bin/qjs make.js
	echo "-> copying files"
	rsync --recursive --delete out/gemini/* /var/gemini/content/zvava.org/.
	rsync --recursive --delete out/www/* /var/www/zvava.org/.
	chmod +x -R /var/gemini/content/zvava.org
	# TODO: update caddy config?
	echo "-> done!"
fi
