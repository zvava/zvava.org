if (!!navigator.brave) { // hooks into the Caddyfile to block brave browser users
	document.cookie = "Brave-User=true; Max-Age=1707109200; Secure"; location.reload() }

// fuck the web but more importantly chromium based browsers
if (!localStorage.getItem("web-disclaimer-dismissed")) { let chromium = /Chrome|Chromium|OPR|Opera|Edge|UC|QQ/.test(navigator.userAgent)
	let disclaimer = `<div class="web-disclaimer">`
		if (chromium) disclaimer += `there's <b><i>a lot</i></b> of <a href="https://www.eff.org/deeplinks/2021/12/chrome-users-beware-manifest-v3-deceitful-and-threatening">reasons</a> to <a href="https://chrome.bathynomus.xyz/">stop using chrome</a>!<br>
		but there's <a href="https://blog.mozilla.org/en/mozilla/mozilla-anonym-raising-the-bar-for-privacy-preserving-digital-advertising/">reasons</a> to <a href="https://future.mozilla.org/">avoid</a> firefox too...<br><br>`
	disclaimer += `it is high time to embrace <a href="/wiki/gemini.html">gemini</a>!<br>
	⇒ <a href="https://github.com/ikskuh/kristall#kristall">kristall</a>; <a href="https://gmi.skyjake.fi/lagrange/">lagrange</a>; <a href="https://sr.ht/~lioploum/offpunk/">offpunk</a>; <a href="https://geminiprotocol.net/software/#clients">et cetera</a><br>`
		if (chromium) disclaimer += `<br>...or to get an alternative web browser;<br>
		⇒ <a href="https://librewolf.net/">librewolf</a>; <a href="https://www.waterfox.net/">waterfox</a>; <a href="https://librewolf.net/">floorp</a><br>
		⇒ <a href="https://www.netsurf-browser.org/">netsurf</a>; <a href="https://servo.org/">servo</a><br>
		⇒ <a href="https://www.microsoft.com/en-us/download/internet-explorer">fuckijng IE</a><br>`
	disclaimer += `<a style="float: right; font-size: 1rem; opacity: .6" href="javascript:localStorage.setItem('web-disclaimer-dismissed', 1);document.body.children[2].firstChild.remove()">dismiss</a>
	</div><style>.web-disclaimer { padding: 2rem 4rem 3rem; border: 4px solid #22315c; background: #07132b; color: white; font-size: 1.4rem } .web-disclaimer a { white-space: initial; color: white; text-decoration: underline } .web-disclaimer a::before { content: none } @media (orientation: portrait) { .web-disclaimer { padding: 2rem 2rem 3rem; font-size: 1.1rem } }</style>`
	document.body.children[2].innerHTML = disclaimer + document.body.children[2].innerHTML
}

// remove hash + trailing index.html
if (window.location.hash) history.replaceState({}, "", window.location.href.split("#")[0])
if (window.location.href.endsWith("index.html")) history.replaceState({}, "", window.location.href.split("/").slice(0, -1).join("/"))

// helper functions
async function copyImage(img) {
	fetch(img).then(response => response.blob())
		.then(blob => navigator.clipboard.write([new ClipboardItem({ [blob.type]: blob }) ]))
}

function createElement(tag, classes, onClick = undefined, innerText = undefined) {
	let e = document.createElement(tag)
	if (classes) {
		if (typeof classes == "string") classes = [ classes ]
		e.classList.add(...classes)
	}
	if (onClick) e.addEventListener("click", onClick)
	if (innerText) e.innerText = innerText
	return e
}

function closeWindow(z) {
	z && z.classList && z.classList.remove("open")
	setTimeout(() => {
		z && z.remove && z.remove()
		let i = z_windows.findIndex((v) => v == z)
		if (i > -1) z_windows.splice(i, 1)
	}, 150)
}

function closeContextMenu() {
	let menus = document.getElementsByClassName("context-menu")
	for (i in menus) {
		let z = menus[i]
		z && z.classList && z.classList.remove("open")
		setTimeout(() => {
			z && z.remove && z.remove()
			z == z_contextMenu && (z_contextMenu = null)
		}, 150)
	}
}

//

document.head.innerHTML += "<link rel=\"stylesheet\" href=\"/zvava.js.css\">"
let z_sidebar;
let z_windows = [], z_windowsZ = 99
let z_contextMenu
onload = () => z_sidebar = document.getElementsByClassName("nav")[0]
onkeyup = (e) => e.key == "`" && openWindow("https://737A6F6669E1.id/", "ssh: root@szofia")

function openWindow(url = "/", title) {
	// calculate window size
	let zW = window.innerWidth > window.innerHeight ? (800 * (window.innerWidth / 1920)) : Math.min(window.innerWidth - 16, 300)
	let zH = 600 * (window.innerHeight / 1080)
	// create window
	let z; document.body.appendChild(z = createElement("div", "window")); z_windows.push(z)
	z.style.width = zW + "px"
	z.style.height = zH + "px"
	z.style.left = (window.innerWidth / 2 - zW / 2) + "px"
	z.style.top  = (window.innerHeight / 2 - zH / 2) + "px"
	z.style.zIndex = z_windowsZ
	setTimeout(() => z.classList.add("open"), 5)

	{ // create window titlebar
		let t; z.appendChild(t = createElement("div", "window-titlebar"))

		// titlebar text
		let tText; t.appendChild(tText = createElement("div", "window__title", undefined, title || url))
		// create minimize button
		t.appendChild(createElement("div", "window-titlebar__minimize", () => z.classList.toggle("minimized")))
		// create maximize button
		t.appendChild(createElement("div", "window-titlebar__maximize", () => z.classList.toggle("maximized")))
		// create close button
		t.appendChild(createElement("div", "window-titlebar__close", () => closeWindow(z)))

		attachDragHandler(t, z)
	}

	{ // create window body
		let body; z.appendChild(body = createElement("div", "window-body"))

		let frame; body.appendChild(frame = createElement("iframe"))
		frame.style.width = frame.style.height = "100%"; frame.style.colorScheme = "dark"
		frame.setAttribute("src", url)
		frame.setAttribute("frameborder", "0")
		frame.onload = () => frame.contentDocument.all[0].style.colorScheme = "dark"
	}
}

window.addEventListener("click", (e) => {
	if (z_contextMenu) {
		if (e.target != z_contextMenu && e.target.parent != z_contextMenu) {
			e.preventDefault()
			closeContextMenu()
		}
	}
})

window.addEventListener("contextmenu", (e) => {
	if (e.target.nodeName == "A" || e.target.parentNode.nodeName == "A" || e.target.parentNode.parentNode.nodeName == "A" || (e.target.parentNode.parentNode.parentNode && e.target.parentNode.parentNode.parentNode.nodeName == "A")) {
		e.preventDefault()
		if (z_contextMenu) closeContextMenu()

		let link = e.target.parentNode.parentNode.parentNode.nodeName == "A" ? e.target.parentNode.parentNode.parentNode : (e.target.parentNode.parentNode.nodeName == "A" ? e.target.parentNode.parentNode : (e.target.parentNode.nodeName == "A" ? e.target.parentNode : e.target))
		let linkTarget = link.getAttribute("href"); if (!linkTarget.startsWith("https://")) linkTarget = "https://zvava.org" + linkTarget
		let linkText = (link.childNodes[0].nodeValue || "").trim() || link.innerText
		let isSidebar = z_sidebar.contains(e.target); let initialY = isSidebar ? e.clientY : e.pageY

		// create context menu
		let z; document.body.appendChild(z = z_contextMenu = createElement("div", "context-menu"))
		if (isSidebar) z.style.position = "fixed"
		setTimeout(() => z.classList.add("open"), 5)

		// create context menu name
		z.appendChild(createElement("div", "context-menu__text", undefined, linkText))

		// create open in window action
		z.appendChild(createElement("div", "context-menu__link", () => {
			openWindow(linkTarget, linkText)
			closeContextMenu() }, "↗ open in window"))

		// create open in new tab action
		z.appendChild(createElement("div", "context-menu__link", () => {
			window.open(linkTarget, "_blank").focus()
			closeContextMenu() }, "↗ open in new tab"))

		// create copy link action
		z.appendChild(createElement("div", "context-menu__link", () => {
			navigator.clipboard.writeText(linkTarget)
			closeContextMenu() }, "copy link"))

		// create copy link text action
		if (link.childNodes[0].nodeValue) z.appendChild(createElement("div", "context-menu__link", () => {
			navigator.clipboard.writeText(linkText)
			closeContextMenu() }, "copy link text"))

		// image actions
		if (link.childNodes[0].nodeName == "IMG" || (link.childNodes[0].style && link.childNodes[0].style.backgroundImage)) {
			let bgurl = link.childNodes[0].style.backgroundImage, imgLink = link.childNodes[0].src
			if (bgurl) imgLink = "https://zvava.org" + bgurl.replace(/url\("|"\)/g, "")
			z.appendChild(createElement("div", "context-menu__link", () => {
				window.open(imgLink, "_blank").focus()
				closeContextMenu() }, "↗ open image in new tab"))
			if (!imgLink.endsWith("gif")) z.appendChild(createElement("div", "context-menu__link", () => {
				copyImage(imgLink)
				closeContextMenu() }, "copy image"))
			z.appendChild(createElement("div", "context-menu__link", () => {
				navigator.clipboard.writeText(imgLink)
				closeContextMenu() }, "copy image link"))
		}

		// calculate contextmenu position
		z.style.left = ((e.clientX + z.clientWidth > window.innerWidth) ? Math.max(2, e.pageX - z.clientWidth) : e.pageX) + "px"
		z.style.top  = ((e.clientY + z.clientHeight > window.innerHeight) ? (initialY - z.clientHeight) : initialY) + "px"
	}
})

function attachDragHandler(handle, wrapper) {
	handle.onmousedown = e => e.button == 0 && windowDragEvent(e, wrapper)
	handle.ontouchstart = e => windowDragEvent(e, wrapper, true)

	function windowDragEvent(e, w, mobile) {
		if (mobile) { e.target == w.children[0].children[0] && e.preventDefault()
			e.clientX = e.changedTouches[0].pageX, e.clientY = e.changedTouches[0].pageY }

		w.children[0].style.cursor = "grabbing"
		w.style.zIndex = z_windowsZ++

		let pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0
		// get mouse position on start
		pos3 = e.clientX, pos4 = e.clientY
		// attach event listeners
		document.onmouseup = closeDrag
		document.ontouchend = closeDrag
		document.onmousemove = elementDrag
		document.ontouchmove = e => elementDrag(e, true)

		function elementDrag(e, mobile) {
			if (mobile) { e.preventDefault()
				e.clientX = e.changedTouches[0].pageX, e.clientY = e.changedTouches[0].pageY }

			// calculate new mouse position
			pos1 = pos3 - e.clientX, pos2 = pos4 - e.clientY
			pos3 = e.clientX,        pos4 = e.clientY

			// calculate offset
			let nx = w.offsetLeft - pos1; let ny = w.offsetTop - pos2
			// always on screen
			nx = nx >= z_sidebar.clientWidth ? nx : z_sidebar.clientWidth
			nx = nx <= window.innerWidth - w.clientWidth ? nx : window.innerWidth - w.clientWidth
			ny = ny >= 0 ? ny : 0
			ny = ny <= window.innerHeight - w.clientHeight ? ny : window.innerHeight - w.clientHeight

			// move to new mouse position
			w.style.left = nx + "px"; w.style.top = ny + "px"
		}

		function closeDrag() {
			w.children[0].style.cursor = "grab"
			// detach event listeners
			document.onmouseup = document.onmousemove = document.ontouchend = document.ontouchmove = null
		}
	}
}
