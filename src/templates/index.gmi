=> /images/header.png welcome to zvava.org ({build_info_summary})

hey, i'm sophia (she/her) but u can call me sophie ^-^!! internet mouse girl. you are currently looking at my brain-out-on-a-table, where i keep a constantly evolving collection of thoughts and projects; the sidebar / wiki links below let you take a peeek. never forget that CYBERPUNK IS NOW and to punch fascists! squeak squeak  ᘛ⁐̤ᕐᐷ

{html_sbcont_start}
## navigation
=> /wiki/category/ 📚 categories
{wiki_pinned}
{html_sbcont_end}
{html_color}

## links
=> https://f.619.hu/@z 🔮 social networking service
=> https://matrix.to/#/%23zvavspace%3Am.619.hu 🌐 matrix space
=> https://git.zvava.org/zvava 💾 open source software
=> https://keyoxide.org/5386D9E7ED1C49BEE15B9F32074FE2D02D45891C 🔏 encryption & proofs
=> https://www.buymeacoffee.com/zvavava 🧀 buy mouse a cheese!

{html_sbcont_start}
## wiki
=> /wiki/ 🗃️ wiki
{wiki_recent}
{html_sbcont_end}

## contact
```
sns      @z@f.619.hu
matrix   @z:m.619.hu
mail      z@zvava.org
discord  @zvava
```
=> /media/zvava.asc 🔒 public key

## exit the wiki
=> /wiki/gemini-sites.ln 🪐 gemini directory

{gmi_webrings}
{html_webrings}

```
「 the internet is not a big truck
     it is a series of tubes!!!1   」
```
{br}

{gmi_buttons}
{html_buttons}

⌘︎︎︎︎❖︎︎︎︎♋︎︎︎︎❖︎︎︎︎♋︎︎︎︎📬︎︎︎︎□︎︎︎︎❒︎︎︎︎♑︎︎︎︎📭︎︎︎︎❍︎︎︎︎♏︎︎︎︎♎︎︎︎︎♓︎︎︎︎♋︎︎︎︎📭︎︎︎︎❍︎︎︎︎□︎︎︎︎⧫︎︎︎︎♎︎︎︎︎📬︎︎︎︎⧫︎︎︎︎⌧︎︎︎︎⧫︎︎︎︎

▖▘▖▖▘▘▖▖▘▌▖▖▘▘▖▘▌▌▌▖▌▖▖▌▘▘▌▘▘▌
