#!/bin/sh

printf "20 text/gemini\r\n"
curl -H "X-Forwarded-For: $REMOTE_ADDR" -d "$(echo $SCRIPT_NAME | sed s/.gmi/.html/)" 0.0.0.0:8001/api/viewcount > /dev/null
cat << EOF
{include_page}
EOF
printf "\n"
