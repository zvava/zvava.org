#!/bin/sh

printf "20 text/gemini\r\n"
request=$(curl -H "X-Forwarded-For: $REMOTE_ADDR" -d "$(echo $SCRIPT_NAME | sed s/.gmi/.html/)" 0.0.0.0:8001/api/viewcount)
viewcount=$(echo "$request" | jq '.views')
cat << EOF |
{include_page}
EOF
tr '\n' '\f' | sed -E "s|\`\`\`([^\`]*)\`\`\`|\`\`\`\1views    $viewcount\n\`\`\`|" | tr '\f' '\n'
printf "\n"
