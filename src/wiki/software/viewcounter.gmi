# ethical view counter
=> /images/t/stats.png thumbnail
```
created  2022/09/17
modified 2024/02/06
category software
```

an ethical website view counter i wrote to deploy on this website

=> /stats.ln 📊 see it in action
=> https://git.zvava.org/zvava/viewcounter source code

> TODO: rewrite viewcounter in wren w/ wren-web
> TODO: generate statistics table for gemini too

## why?
i wanted to have really basic analytics (if you can even call it that) on my website that A. don't track you and B. can be easily disabled by just disabling javascript

## how
in about 130 lines of vanilla node.js, viewcounter listens to requests on a single path

if it is a GET request, a summary of all the page view counts are returned

if it is a POST request with a page path as a body, that page's view count is incremented

## preventing abuse
there are two mechanisms in place to prevent abuse of the system

firstly, the url is validated to make sure it's an actual url on the website and not idk a slur or something, of which the result is cached so further requests to the same url can be quickly processed

there is also a 24h ratelimit in place to prevent spam. each ip has a "profile" of when it has last visited a page, if that date is today* then their view is not counted, otherwise the view count is incremented and the date is set to today*

*today in time of the server

## deployment
i use caddy to not only act as the web server, but also a reverse proxy to expose viewcounter at /api/viewcounter. it also automagically provisions ssl certificates the site and its subdomains!

=> https://caddyserver.com/ caddy
