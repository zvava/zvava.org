# rainmeter clicker
=> /images/t/rm-clicker.png thumbnail
```
created  2018/08/08
modified 2024/01/08
category rainmeter
```

an idle game about rainmeter, as a rainmeter skin

=> /wiki/category/rainmeter.ln rainmeter

=> /media/software/rm-clicker_v1.0.rmskin

## generator script
there is a nodejs generator script included to generate all the upgrades/buildings buttons and logic from a file called upgrade.json. i don't know why i gave the values these specific names and why they are only one character in the json but i guess it made sense to me back when i was like eleven. there is also no way to see these values "in game" so you have to keep trying upgrades until you can get them

* `n` — upgrade name
* `i` — iterator type (points per click/points per second)
* `r` — required points (cost)
* `g` — gives
```
[
	{
		"n": "Illustro Modification",
		"i": "ppc",
		"r": "10",
		"g": "1"
	},
	...
]
```
