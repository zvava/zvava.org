# samsung galaxy a52s 5g
```
created  2021/09/01
modified 2023/07/06
category hardware
```

i got one of these after my s10e died after i noticed it was a phone with a headphone jack, 120hz display, supported builds of lineage os 20, and cost about the same as the s10e last year

one thing to note is it is over half an inch wider, and an inch taller, so it is not as suitable for small hands — kind of a pain with average hands too but you get used to it

also the relatively new optical fingerprint sensor takes noticeably longer to authenticate then the refined capacitive fingerprint reader. plus you have to wake up the phone or wait a bit if you don't get it unlocked first time

=> /wiki/phone-s10e.ln samsung galaxy s10e
=> /wiki/lineageos.ln lineage os
=> https://www.gsmarena.com/samsung_galaxy_a52s_5g-11039.php gsm arena

## cost
on average you can find one unlocked second hand for ~300€, about the same as the s10e

### repaired
compared to the s10e, screen repairs cost a bit less but are counteracted by the higher cost of other upgrades — it being more costly to replace the back plastic of the A-series then the back glass of the S-series

## spec breakdown
### pros
* 6.5" 120hz super amoled 20:9 always-on display, tiny bezel
* has deprecated features: headphone jack, fingerprint, microsd slot
* 128/256gb internal storage 4-8gb ram
* plastic back

### neutral
* under screen optical fingerprint sensor
* gyro and gestures
* nfc, bluetooth 5.0
* 25W type C usb 2.0 port (otg)
* mid cameras, has ultrawide, macro, and depth
* dust/water resistance (IP68)
* not as easy enough to install a custom rom onto

### cons
* missing bixby/extra button, missing double tap to wake
* fingerprint sensor is slow
* no wireless charging
* can't access macro or depth camera on custom rom (at least not through open camera)
* VoLTE won't work with custom rom
* warning message for unlocked bootloader on every power up
