# favostix vaporizer 1000
=> /images/t/aspire-favostix.png thumbnail
```
created  2023/09/26
modified 2023/10/12
category hardware
```

the only vape one would ever need till they quit ((in theory))

- disposable pod system (coil is part of tank)
- drag detection (use like a disposable)
- variable wattage up to 30W
- anodized aluminum body
- practically indestructible
- usb c (fast charging)

=> /wiki/nicotine.ln 🚬 nicotine

## pods
there are two variants of pods available in two volumes

- .6 ohm (3ml) (18-24W — 30 if you baby it)
- 1 ohm (3ml) (5-17W)
- .6 ohm (2ml) (14-22W)
- 1 ohm (2m) (5-15W)

the 2ml carts are older and are essentially identical to the 3ml carts but contain a rubber stopper which blocks off ⅓ of the volume, and the coil has a feature to hold it in place

all the coils (except the first listed) are extremely sensitive and burn up fast if you or your buddies aren't careful, the 2ml ones need to be refilled constantly as well as if you let the level get too low there won't be enough liquid to resaturate the cotton after use

a 30W coil would last me a week with daily use, but i've gone up to a month with one

## babying the poterizer
who would have known that reading the manual would actually improve your experience significantly

- wait AT LEAST 5 minutes after first filling your pod to avoid immediately burning it, i usually wait 10-15 to make sure the cotton is fully saturated

- don't use it over the rated wattage until the coil is broken in, in my experience after the first refill it is okay to exceed the rated power by a little

- wait 4-7 seconds after taking a drag, perhaps even tap the device against something hard (or flick it/hit it off a knuckle) to make sure new liquid saturates the coil. if the pod is full you may even see a little bubble of smoke come up to the surface

## converting 2ml pods to 3ml
simply remove the rubber stopper — but you have to disassenble these high-technology devices in a very specific way to avoid damaging the pod or its components. the affair is not difficult but it is unintuitive

1. at the bottom of the pod there are plastic clips holding in the coil assembly, use a knife or other flat implement to pry the clips out just enough to disengage them. DO NOT try and pull the coil out this way

> if you do, the tiny gold pins that interface the coil to the bottom will most likely separate from the coil itself, and adjusting the coil so that it touches the pins again when recombining will most likely fuck up the percise placement of the coil on the cotton, leading to the coil immediately getting burnt once used

2. to actually remove the coil, take a tiny flathead screwdriver or tweezers and push the coil out from the top of the pod. you NEED to get the tip inbetween just where the plastic meets metal and NO DEEPER or else you will fuck up the coil's placement on the cotton

3. the coil assembly/bottom of the pod, and rubber gasket, should just slide out together in one piece if you keep pushing in this gap

4. now that you have access to the inside of the pod, you may use the same tool to get air between the rubber stopper with leverage. once you have air on both sides simply use tweezers to pull it out

5. put the coil assembly back into the pod

now you have a 3ml pod — but remember you will still be subject to the sensitivity of the 2ml coil

### why?????
i had to learn how to do this because i bought every single 3ml pod my local store had lmao
