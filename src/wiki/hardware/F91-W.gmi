# casio F91-W
=> /images/t/F91-W.png thumbnail
```
created  2023/05/30
modified 2023/05/31
category hardware
```

the casio f91-w is the classic inexpensive digital watch from casio used for bomb-making, known for its
* singular backlight led
* singular daily alarm
* stopwatch
* very low-power
* simplicity 😊
* longterm time accuracy
* ability to be easily modded 🤩

# modification
=> /media/picture/f91-w.png modification thumbnail

## sensorwatch
sensorwatch makes a board replacement for the watch that makes use of all the existing parts of the watch -- so you get an ARM processor with full control over display segments and gpio extensibility, retaining the battery life and good timekeeping of the classic
=> https://www.sensorwatch.net/ sensorwatch

it retails $39 for the board itself and $4 for the temp + gpio breakout pcbs (shipping for $8 us/$18 worldwide)

## n-o-d-e
very inceteful resources on f91-w modding that summarizes a lot of common and some unique mods
=> https://n-o-d-e.net/datarunner.html datarunner
=> https://n-o-d-e.net/watch_mods.html misc. watch mods

### datarunner
* front acrylic replacement
* embedded nfc/rfid antenna
* 3d printed insert for micro-sd
* experiment to embed a micro-sd usb reader
* replacing the backing film of the lcd
* experiment to replace piezo with vibration motor

### misc. watch mods
* putting transparent colored polymide tape on lcd to change background color
* putting polarized sheet on to lcd to invert colors
* removing the backing film of the lcd and painting the lcd white
* replacing the backlight (and adding a secondary on the other side)

## my own
i attempted to 3d print a larger back for the watch that could house an attiny85, a female usb c port, and gpio headers; the idea being it could act as a portable rubber ducky or extended with a module with a battery for the microcontroller to do stuff and thing

=> https://merveilles.town/@zvava/110321194037342694 🧵 mastodon project thread

it failed after i could not successfully solder tiny wires free hand without a protoboard AND keep the connections from eventually short-circuiting or breaking completely :c
