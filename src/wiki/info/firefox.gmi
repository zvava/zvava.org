# firefox
=> /images/t/firefox.png thumbnail
```
created  2024/05/06
category info, internet
```

configuration of firefox
=> https://www.mozilla.org/en-US/firefox/new/ official website

if you don't feel like doing too much configuration, you should try out librewolf
=> /wiki/librewolf.ln 🐺 librewolf

## animated galaxy theme
you should definitely at least try it out it is so pretty and cool!!!!
=> https://addons.mozilla.org/en-US/firefox/addon/purple-starfield-animated purple starfield - animated

## compact mode
mozilla removed the compact density after the redesign, to add it back go to `about:config` and toggle the `browser.compactmode.show` option. now you can select it in the customize page again

## disable mozilla telemetry
in general settings, scroll down to "browsing" and uncheck the "recommend extensions/features as you browse" options

in settings > home, set new windows and tabs to "blank page", and just in case disable all the checkboxes under "firefox home content" too

in settings > privacy & security, scroll to "firefox data collection and use" and disable all of the checkboxes

## privacy hardening
block ads!
=> https://addons.mozilla.org/en-US/firefox/addon/ublock-origin ublock origin

block cookie pop-ups!
=> https://addons.mozilla.org/en-US/firefox/addon/istilldontcareaboutcookies i still don't care aobut cookies

remove tracking content from urls (eg. ?utm_source=, &utm_medium=, &utm_content=, etc.)
=> https://addons.mozilla.org/en-US/firefox/addon/clearurls clearurls

now if you want to go the extra mile, there's chameleon. it lets you spoof your user-agent, as well as many other fingerprinting-resistance features like spoofing the output of certain javascript commands. chameleon will break websites but you can add exceptions
=> https://addons.mozilla.org/en-US/firefox/addon/chameleon-ext chameleon

### enhanced tracking protection
firefox comes with an amazing tool called enhanced tracking protection. go to settings > privacy & security, choose the "custom" option, set cookies to "all cross-site cookies", and tracking content to "in all windows". if you use the dark mode, keep the suspected fingerprinters on only in private windows, and use the following for fingerprint resistance instead:

### resist fingerprinting w/ dark theme
to enable fingerprinting resistance go to `about:config` and toggle the `privacy.resistFingerprinting.pbmode` flag to true, and set `privacy.fingerprintingProtection.overrides` to "-CSSPrefersColorScheme" too

### clear cookies on exit
one of the best things you can do for your privacy is clear the cookies of all unknown websites when you close the browser. go to settings > privacy & security > cookies and site data, enable "delete cookies and site data when firefox is closed", and go into "manage exceptions" to whitelist sites you regularly use

there's also an extension which lets you do this on the closure of tabs as well
=> https://addons.mozilla.org/en-US/firefox/addon/cookie-autodelete cookie autodelete

## containers
firefox has a unique feature called containers, which are a way to sandbox cross-site cookies, session data, etc. between websites

sandbox large companies & monopolies
=> https://addons.mozilla.org/en-US/firefox/addon/facebook-container facebook container
=> https://addons.mozilla.org/en-US/firefox/addon/google-container google container
=> https://addons.mozilla.org/en-US/firefox/addon/contain-amazon amazon container

manually assign websites to containers
=> https://addons.mozilla.org/en-US/firefox/addon/containerise containerise
* you can assign your search engine to an "untrusted" container, so general web browsing is sandboxed
* you can sandbox your banking websites, retail outlets, or anything that handles sensitive information like payment details
* sandbox microsoft as well!
```
@(.+)?azure.com , Microsoft
@(.+)?bing.com , Microsoft
@(.+)?github.com , Microsoft
@(.+)?github.io , Microsoft
@(.+)?microsoft.com , Microsoft
@(.+)?office.com , Microsoft
@(.+)?visualstudio.com , Microsoft
@(.+)?windows.com , Microsoft
```

## misc. useful extensions
show if a website author links their fediverse page
=> https://addons.mozilla.org/en-US/firefox/addon/streetpass-for-mastodon streetpass

custom css (usercss manager)
=> https://addons.mozilla.org/en-US/firefox/addon/styl-us stylus

custom js (userjs manager)
=> https://addons.mozilla.org/en-US/firefox/addon/greasemonkey greasemonkey

custom redirections manager
=> https://addons.mozilla.org/en-US/firefox/addon/redirector redirector

button to translate the current website
=> https://addons.mozilla.org/en-US/firefox/addon/traduzir-paginas-web translate web pages

manage tabs vertically, with focus on containers
=> https://addons.mozilla.org/en-US/firefox/addon/container-tabs-sidebar container tabs sidebar

a crowdsourced effort to skip sponsors in youtube videos just like ads
=> https://addons.mozilla.org/en-US/firefox/addon/sponsorblock sponsorblock

add the dislike count back to youtube
=> https://addons.mozilla.org/en-US/firefox/addon/return-youtube-dislikes return youtube dislike
