# syncthing
=> /images/t/syncthing.png thumbnail
```
created  2022/09/22
category info
```

backup your files using the storage you already have

=> https://syncthing.net/ official website

## syncing other data
### android backup
if you are running lineage os or a rom based off it you can simply set up backups to the internal storage (or external if you have an sd card!) and create a sync to sync to the .SeedVaultAndroidBackup folder in the root of the location

### caldav (calendar, phonebook, and tasks)
=> /wiki/decsync.ln decsync

## troubleshooting
### folder path
you cannot change a folder's path once it is added to a device, you must remove the folder from the device and then import it from another device

it *is* possible, although discouraged, to
1. stop syncthing
2. move the files to the new directory
3. change the path in the syncthing configuration file
4. restart syncthing

(you should only do this if you cannot afford to delete and resync a large amount of data)

### error: "folder marker missing"
this means the `.stfolder` directory is missing in the directory path. the error message does not say this as this could potentially indicate data loss. once you are sure nothing is missing then you can just create a folder called `.stfolder` in your synced directory and rescan to solve this error message

## installing
=> https://syncthing.net/downloads/ binary downloads

### linux
on linux you can just install the package from your package manager

you can run it as a systemd (or whatever your init system is) service, or otherwise run an instance of it on start up, and then access it via. the web dashboard

### android
there is a handy syncthing wrapper app for android available

i recommend configuring the run conditions to preserve battery life and data. you will probably also be prompted to let it run unrestricted in the background which you should permit

=> https://github.com/syncthing/syncthing-android syncthing-android

### windows
on windows you can install synctrazor to provide a neat little tray icon to represent the running process. it even includes the web dashboard and output log of syncthing in the window that opens when you click on it

if you are syncing from *nix machines, it is worth noting that due to ntfs fuckiness you might need to change a few filenames from those systems as ntfs has more characters disallowed in filenames then ext does

=> https://github.com/canton7/SyncTrayzor synctrayzor

### mac
i don't have a mac. good luck

=> https://github.com/syncthing/syncthing-macos syncthing-macos

### ios
you are better off not installing syncthing on this platform as A. the only available option(s?) are paid and B. ios doesn't provide a usable file system
