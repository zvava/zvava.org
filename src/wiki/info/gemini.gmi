# the gemini protocol
=> /images/t/gemini.png thumbnail
```
created  2023/06/01
modified 2024/03/11
category info, internet
```

gemini is a protocol based on HTTPv1 and gopher with the following features;
* minimal specification with focus on simplicity and low-power computing/low-speed networking
* input via uri query
* redirects (including cross-platform, though discouragesd)
* designed with privacy in mind
* mandatory tls (permitting self-signing)
* simplified markdown variant (gemtext) for presenting information
* no more features

=> stay://geminiprotocol.net project website
=> /wiki/gemini-sites.ln 🪐 gemini directory

zvava.org's articles are written in gemtext and also served through gemini with a static gemini server called agate

=> /wiki/website-rewrite2022.ln website rewrite (2022)

## servers
there are many options available, these are two that i like and are written in rust. they're both really simple, but agate requires less config. you should use stargazer if you know you want to do stuff with cgi scripts

=> https://github.com/mbrubeck/agate agate
=> /wiki/stargazer.ln 🔭 stargazer
=> stay://geminiprotocol.net/software/#servers misc servers

## browsers
there are many options available, ranging from tiny terminal clients to full gui clients with certificate management! i personally use kristall on a desktop but lagrange on a phone

=> https://kristall.random-projects.net/ kristall (gui)
=> https://gmi.skyjake.fi/lagrange/ lagrane (gui)
=> https://github.com/makew0rld/amfora amfora (tui)
=> https://sr.ht/~lioploum/offpunk/ offpunk (tui)
=> stay://geminiprotocol.net/software/#clients misc clients

## gemtext cheatsheet
the following has "preformatted" text (code blocks) by placing said text inside of a code block which is two pairs of ``` alone on one line

headings are pretty self-explainatory
```
# large heading
## heading
### small heading
```
gemini comes with one form of list;
```
* unordered list
* another item
```
but zvava.org supports ordered lists too!
```
1. ordered list
2. item two
```
there is the blockquote to add notes or quotes
```
> quote
```
lastly a link (or embedded attachment on zvava.org if to media) is defined by a ⇒, link, and optionally its alt text
```
=> /relative/link.gmi also look at this
```

## power use
although the minimalism of gemini may seem dissuading to the idea of the 'web app', you can make useful, secure, and intricate ones with gemini by being creative with the tools you are given

you can create and use client certificates in most gemini browsers for authentication or session persistence. since client certificates are not required to access most surface content, there are status codes to ask you to use one or tell you if you are on an incorrect certificate

you can send small amounts of data in input requests via uri query, and can even request input from the user directly, for example;
> 1. gemini://search-engine.com/search → response specifies input requirement (response status code 10/11)
> 2. user provides input → /search?big%20boobs (request status code 1)
> 3. server responds with result (response status code 20) or permanent error (code 50/59)

it is possible to do the above, and many other things, through the use of "cgi scripts". cgi pages are generated from scratch through a script ran for every request, kind of like php

```
#!/bin/sh

printf "20 text/gemini\n"
printf "\`\`\`\n"
printf "$(uptime -p | cowsay -f hellokitty)\n"
printf "\`\`\`\n"
```

## drawbacks
naturally there are a lot. no support for caching, compression, or resuming interupted downloads, and there is no header information so the client does not know what size the file it is receiving is supposed to be. this is why very large files are not suitable for gemini as they are susceptible to being corrupted
