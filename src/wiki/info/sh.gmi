# shell
=> /images/t/terminal.png thumbnail
```
created  2023/06/01
modified 2023/07/08
category info
```

notes on shell scripting

=> /wiki/unix.ln *nix
=> /wiki/batch.ln batch

## shebang
a shebang or hangbang is the character sequence at the beginning of a script denoting what interpreter to use, it is a hashtag followed by an exclaimation mark then the path to the executable of the interpreter of the scripting language in the format `#!interpreter [args]`

so if we have script.sh with a shebang to call the sh binary with a few arguments

> #!/bin/sh -x -y
> echo Hello World!

when we run it with

> ./script.sh a b

it gets executed as

> /bin/sh -x -y ./script.sh a b

### examples

#!/usr/bin/env sh
> → use the bourne shell, most portable

#!/bin/sh
> → use the bourne shell

#!/bin/bash
> → use the bourne again shell

#!/usr/bin/pwsh
> → use powershell

#!/bin/false
> → do nothing when called by a user and return a non-zero exit code

## bourne vs bourne again shell
the simple difference between sh and bash is that sh is completely portable across all *nix distributions and bash has a few extensions from gnu added to it, there are also other shells that are less common with more liberal extensions

## cheat sheet
TODO: write shell script language cheatsheet
