# wren scripting language
=> /images/t/wren.png thumbnail
```
created  2023/08/12
modified 2024/02/06
category info
```

wren meets all design goals my dream language would. c-style, simple to learn, and actually very very useful

=> https://wren.io/ official website

TODO: cheatsheet
TODO: useful libraries and examples

## wren-web
wren-web is my fork of the defunct spiderwren networking library

=> /wiki/wren-web.ln wren-web
=> https://spiderwren.nailuj29gaming.com/ spiderwren

## dome
dome is a game framework that allows you to write graphical applications in the simple but powerful wren scripting language

=> /wiki/dome.ln dome engine
