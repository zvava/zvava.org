# cookie clicker - fortunes
```
created  2022/05/29
modified 2023/07/12
category info
```

in cookie clicker, there is a heavenly upgrade called "- fortune cookies". once unlocked, they will make random - fortunes appear in the new ticker which, when clicked, unlock a special upgrade. - fortunes 1 through 18 are each related to a building, providing a 7% boost in production and a 7% reduction in price

=> https://cookieclicker.fandom.com/wiki/Cookie_Clicker_Wiki cookie clicker wiki

i find some of these - fortune messages pretty interesting, my favorites are listed here:

> The seeds of tomorrow already lie within the seeds of today.
- fortune #003 (farms)
{br}

> True worth is not in what you find, but in what you make.
- fortune #005 (factories)
{br}

> The value of money means nothing to a pocket.
- fortune #006 (banks)
{br}

> Not all guides deserve worship.
- fortune #007 (temples)
{br}

> Every mile travelled expands the mind by just as much.
- fortune #009 (shipments)
{br}

> Don't get used to yourself. You're gonna have to change.
- fortune #010 (alchemy labs)
{br}

> Do your future self a favor; they'll thank you for it.
- fortune #012 (time machines)
{br}

> The world is made of what we put into it.
- fortune #013 (antimatter condensers)
{br}

> Staring at a dazzling light can blind you back to darkness.
- fortune #014 (prisms)
{br}

> Don't leave to blind chance what you could accomplish with deaf skill.
- fortune #015 (chancemakers)
{br}

> It's good to see yourself in others. Remember to see yourself in yourself, too.
- fortune #016 (fractal engines)
{br}

> If things aren't working out for you, rewrite the rules.
- fortune #017 (javascript consoles)
{br}

> There's plenty of everyone, but only one of you.
- fortune #018 (idleverses)
{br}

> The smartest way to think is not to think at all.
- fortune #019 (cortex bakers)
{br}

> No matter how hard you try, you're never truly alone.
- fortune #020 (you)
{br}

> True wealth is counted in gifts.
- fortune #100
{br}

> Don't believe the superstitions; all cats are good luck.
- fortune #103
