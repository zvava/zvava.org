# gemini directory
=> /images/t/gemini.png thumbnail
```
created  2024/03/11
modified 2024/06/23
category internet
```

a directory of neat gemini capsules i have discovered

=> gemini://zvava.org/
=> gemini://geminiprotocol.net
=> /wiki/gemini.ln 🌌 gemini

## $ cat list.gmi
=> gemini://astrobotanry.mozz.us astrobotany
=> gemini://problemfox.pollux.casa/ problem fox
=> gemini://qwertqwefsday.eu/ johann's gemlog
=> gemini://rwv.io/ rwv's capsule
=> gemini://vierkantor.com/ virekantor / Anne Baanen
=> gemini://old-home.faith unconscious / wren'
=> gemini://m0xee.net/ mYe pl0xACe
=> gemini://mediocregopher.com/ mediocregopher's blog
=> gemini://typed-hole.org typed-hole.org
=> gemini://mozz.us mozz.us

### BBSs
=> gemini://bbs.geminispace.org/ geminispace
=> gemini://station.martinrue.com/ station

### tildes
=> gemini://zaibatsu.circumlunar.space/ zaibatsu
=> gemini://gemlog.blue/ gemlog blue
=> gemini://sdf.org/ sdf pubnix
=> gemini://tilde.team/ tilde team pubnix

### misc
=> gemini://gemini.bortzmeyer.org/software/lupa/lupa-capsules.gmi lupa (gemini crawler)

## bonus gopher sites
=> gopher://tengu.space DeviousTengu's space
=> gopher://typed-hole.org typed-hole.org
=> gopher://mozz.us mozz.us
=> gopher://zaibatsu.circumlunar.space/ zaibatsu
