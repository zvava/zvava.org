# zvava.org
=> /images/t/newtab.png thumbnail
```
created  2023/06/01
modified 2024/05/05
category text
```
{br}
> as much as i would love to have this place be a complete resource, it is far from it. there are so many untold stories, and so many more that will stay like that forever

about this website and how it is constructed

=> /feed.ass 📃 wiki feed
=> https://zvava.org 🕸️ view html version
=> https://git.zvava.org/zvava/zvava.org 📦 source code

## dependencies
=> /wiki/qjs.ln 💿 bellard quickjs
=> /wiki/gemini.ln 🌌 the gemini protocol
=> /wiki/viewcounter.ln 🧮 viewcounter
=> /wiki/hue-server.ln 🎨 hue server
=> /wiki/ass.ln 🍑 actually simple syndication

## history
=> /wiki/website-refresh2024.ln refresh - 2024
=> /wiki/archiving-2024.ln archiving - 2024
=> /wiki/website-refresh2023.ln refresh - 2023
=> /wiki/archiving-2022.ln archiving
=> /wiki/lazymode.ln lazy browsing mode
=> /wiki/website-rewrite2022.ln rewrite - 2022
=> /wiki/introducing-notes.ln begin wiki - 2021
=> /wiki/website-rewrite2020.ln rewrite
=> /wiki/archiving-2020.ln archiving
=> /wiki/website-redesign2020.ln redesign - 2020

## architecture
this website is generated every time it is updated by a static site generator. basically, all the files in the source like articles and layout templates get built into a final 'output' which can then be simply copied to a static file server. the advantages over the web app paradigm which i used before are numerous, but the most important come down to being simple, and being able to be crawled/indexed & archived no problem

### server
zvavnet itself is runs on the cheapest vps known to man, the 23$/yr lowest-tier vps from racknerd.com, equivalent specs to a raspberry pi zero but the single vcore on the vps is faster. it is running caddy (for http/https), stargazer (for gemini), and fast reverse proxy. there are several services exposed on zvavnet with caddy that are forwarded from my home network with frp

=> /wiki/caddy.ln caddy
=> /wiki/frp.ln frp (fast reverse proxy)
=> /wiki/stargazer.ln stargazer

### wiki utility script
there is a wiki.js file which contains several tools for wiki contributors, it has documentation built into the command but it is also available here
=> https://git.zvava.org/zvava/zvava.org#contributing wiki.js usage instructions

> TODO: rewrite wiki utility script in wren

### generator script
the generator script (make.js) is located at the root directory and requires a unix system to run as quickjs does not support executing arbitrary commands on windows. this script crunches all the data, formats everything nicely, and outputs static files that can be served all in few hundred lines of vanilla javascript (463 as of 24/03/15)

0. main entry point
> → create or clean output directory
1. fetchTemplates
> → read and cache all templates
2. fetchWiki
> → read and cache all wiki pages
> → parses and caches metadata for each page too
3. generateTemplates
> → fills out dynamic templates, and saves to cache
4. generateGemini
> → generate all final gemtext
5. generateHTML
> → translate all gemtext into final html
6. generateAss
> → generate an .ass feed of non-stub articles
{br}
> TODO: rewrite generator script in wren
