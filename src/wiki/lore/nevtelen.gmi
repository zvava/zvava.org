# névtelen
```
created  2023/10/18
category text
```

while being kind of shy about it and not being fully settled on a new legal name, i changed my first name online in various places to névtelen. in direct translation from hungarian this is a combination of the root words "name" and "without", so "without name", and often in translations you would see it in the place of "untitled". i've managed to title myself something nice which makes me happy, so i have mostly deprecated the name since

## see also
=> /wiki/minin-zvava.ln minin → zvava
