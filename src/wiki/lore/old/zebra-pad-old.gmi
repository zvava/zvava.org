# zebra pad
=> /images/t/zebrapad.png thumbnail
```
created  2017/03/13
category software
```

zebra pad is my notepad app originally made in pika software builder on march of 2017 but "remastered" in C#.net winforms on the august of 2017. it utilized murpe for updates.
=> /wiki/pika-software-builder.ln pika software builder
=> /wiki/murpe-update-old.ln murpe
=> /media/software/zebrapad.zip

version 1.0 features a big textbox for your words and a custom window bar with an update button on the left which linked to zebra pad's murpe update page, a text bar for naming the file in the center, and mac os styled stoplights on the right. the green button saved the file in documents with the name given in the textbox, the yellow button minimizes the application to taskbar, and the red button cleared the content textbox.

version 1.1 was built on the same day as 1.0 and removes the file name textbox as it didn't work. it just saves a 'New Text Document.txt' in your documents folder lmfao

version 1.2 — basically 2.0 though — was a complete revamp in C#.net with winforms for gui, and im still a little proud of it to this day - back then i was proud of it enough to make a whole alternativeto.net page for it. if you want some screenshots of how it looked like they're there, email me if they've taken it down cause i bet it's not long until they discover it's discontinued. :P i have archived the screenshots on there in the download link too in case
=> https://alternativeto.net/software/zebra-pad/ zebra pad's alternativeto.net page
