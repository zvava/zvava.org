# introducing notes
=> /images/t/notes.png thumbnail
```
created  2021/07/05
modified 2023/06/01
category text
```

i have added an additional section to this website where i will share some 'notes' which don't fit into the blog format and act more as wiki pages. i use these pages to deliver information that i may update sometime in the future, for example a list of books i've read. it has it's own spot below the blog on the front page of the website showing the last modified page.

=> /wiki/

## 2023
notes have become the premise of the website and there are very few non-notes anymore, this page is merely a memory the time long since gone
