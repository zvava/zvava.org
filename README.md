# zvava.org
static website generator (html and gemini) for my personal website [zvava.org](https://zvava.org)

## [to do] rewrite no. 4
re write the whole website again but in [wren](https://wren.io) and yeah

## contributing
the `qjs wiki.js` utility is provided to help me (and you!) to create modifications to the wiki. it has usage information built in, the most useful of which are `new` and `make`. to make development faster you may `alias wiki="qjs wiki.js"`

### new page
create a new wiki page with placeholder content

`usage: qjs wiki.js new page [args...]`

general arguments:
* `-h  --help`: display usage information
* `-f  --force`: don't abort if page already exists

add metadata:
* `-t  --title`:       set new page's title
* `-C  --created`: set new page's created date
* `-m  --modified`: set new page's modified date
* `-T  --thumb`: set new page's thumbnail
* `-c  --category`: add a category to the page

add content:
* `-h1 --header`: add a # header to the page
* `-h2 --sub`: add a ## header to the page
* `-h3 --subsub`: add a ### header to the page
* `-p  --text`: add a paragraph to the page
* `-q  --quote`: add a block quote to the page
* `-l  --link`: add a link to the page
* `-i  --image`: add an image to the page
* `-a  --alt`: set alt text of previous image/link

## development
run `qjs make.js` (or `wiki make` if you have the alias) to create out/, out/gemini/, and out/www/, then generate contents

run `webserver 8080 out/www/` (`npm i -g webserver`) to debug html output
https://github.com/mbrubeck/agate
> TODO: find out how to debug gemini now that every page is a cgi script

## production
make sure all changes are committed and pushed

run `qjs wiki.js publish` and enter password to sync changes with the webserver

remote server will fetch the latest version of this repo, and run the publish.sh script. this builds the site remotely and copies `out/gemini/` to `/var/gemini/content/` and `out/www/` to `/var/www/`and `/var/gemini/content`
